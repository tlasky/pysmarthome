import json
from urllib.parse import urljoin
from requests import Session, Response


def create_url(*parts) -> str:
    return urljoin(
        'https://pysmarthome.herokuapp.com',
        # 'http://localhost:5000/',
        '/'.join(list(map(str, parts)))
    )


def create_api_url(*parts) -> str:
    return create_url('api', 'v1', *parts)


def print_response(response: Response):
    print(response.status_code, response.request.method, response.url)
    print('Headers:', json.dumps(dict(response.headers), indent=4))
    try:
        print('Body:', json.dumps(response.json(), indent=4))
    except json.JSONDecodeError:
        print('Body:\n', response.text)


session = Session()
session.headers.update({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
})

login_response = session.post(
    create_url('login'),
    data=json.dumps(dict(email='testuser@example.com', password='123*'))
)
print_response(login_response)
session.headers.update({
    'Auth-Token': login_response.json()['response']['user']['authentication_token']
})

print_response(session.get(create_api_url('device')))
print_response(session.get(create_api_url('device/1')))

add_device_response = session.post(
    create_api_url('device'),
    data=json.dumps(dict(
        amazon_type=['LIGHT'],
        google_type='action.devices.types.LIGHT',
        name='test light',
        description='Test light device.',
        custom_data=dict(
            test_data=None
        )
    ))
)
print_response(add_device_response)

print_response(session.post(
    create_api_url('trait', add_device_response.json()['device']['id']),
    data=json.dumps(dict(
        readable=True,
        settable=True,
        mqtt_get='/pysmarthome/test/ping/get',
        mqtt_handler='string',
        custom_data=dict(
            test_data=None
        )
    ))
))

for _ in range(2):
    print_response(session.delete(
        create_api_url('device', add_device_response.json()['device']['id'])
    ))

print_response(session.post(
    create_api_url('device'),
    data=json.dumps(dict(
        description="temperature sensor",
        id=2,
        name="outside sensor",
        online=True,
        uid="a10e17b1-05b5-4d38-93cb-e4933be09b74",
    ))
))
