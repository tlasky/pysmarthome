from typing import *
from flask.json import JSONEncoder
from datetime import datetime


def correct_range(x: float, x_min: float, x_max: float) -> float:
    """
    Correct value to be with in specified range
    """
    if x < x_min:
        return x_min
    elif x > x_max:
        return x_max
    return x


def range_map(x: float, in_min: float, in_max: float, out_min: float, out_max: float) -> float:
    """
    Map number by specified ranges
    """
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


def split_string_list(data: str, separator: str = ';', strip: bool = True) -> List[str]:
    """
    Split string to list of string by separator
    """
    return [
        item.strip() if strip else item
        for item in data.split(separator)
        if strip and item.strip()
    ]


def join_string_list(data: List[Any], separator: str = '; ') -> str:
    """
    Join list of strings with separator
    """
    return separator.join(list(map(str, data))) if data else str()


class CustomJSONEncoder(JSONEncoder):
    """
    Json encoder
    """

    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        elif hasattr(obj, 'to_dict'):
            return obj.to_dict()
        return super().default(obj)
