from application.models import OauthClient, OauthToken, OauthCode
from application import app, db
from authlib.oauth2.rfc6749 import grants
from authlib.oauth2.rfc6750 import validator
from authlib.integrations.flask_oauth2 import ResourceProtector
from datetime import datetime


class AuthorizationCodeGrant(grants.AuthorizationCodeGrant):
    TOKEN_ENDPOINT_AUTH_METHODS = [
        'client_secret_basic',
        'client_secret_post',
        'none',
    ]

    def save_authorization_code(self, code: OauthCode, request):
        auth_code = OauthCode(
            code=code,
            client=request.client,
            user=request.user,
            redirect_uri=request.redirect_uri,
        )
        db.commit()
        return auth_code

    def query_authorization_code(self, code: OauthCode, client: OauthClient):
        auth_code = OauthCode.get(code=code, client=client)
        if auth_code and not auth_code.is_expired():
            return auth_code

    def delete_authorization_code(self, authorization_code: OauthCode):
        authorization_code.delete()
        db.commit()

    def authenticate_user(self, authorization_code: OauthCode):
        return authorization_code.user


class BearerTokenValidator(validator.BearerTokenValidator):
    def authenticate_token(self, token_string: str) -> OauthToken:
        return OauthToken.get(access_token=token_string)

    def request_invalid(self, request):
        pass

    def token_revoked(self, token):
        pass


def query_client(client_id: str) -> OauthClient:
    return OauthClient.get(uid=client_id)


def save_token(token_data: dict, request):
    app.logger.debug(f'Oauth {request.client.name} for user {request.user.email} is {token_data["access_token"]}')
    OauthToken(
        client=request.client,
        user=request.user,
        token_type=token_data['token_type'],
        access_token=token_data['access_token'],
        expires_in=datetime.fromtimestamp(token_data['expires_in'])
    )
    db.commit()


oauth_required = ResourceProtector()
oauth_required.register_token_validator(BearerTokenValidator())
