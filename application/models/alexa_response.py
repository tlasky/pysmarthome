import uuid
import time


class AlexaResponse:
    """
    https://github.com/alexa/skill-sample-python-smarthome-switch/blob/master/
    lambda/smarthome/alexa/skills/smarthome/alexa_response.py
    """

    def __init__(self, **kwargs):
        self.context_properties = []
        self.payload_endpoints = []
        self.context = {}
        self.cookies = {}
        self.event = {
            'header': {
                'namespace': kwargs.get('namespace', 'Alexa'),
                'name': kwargs.get('name', 'Response'),
                'messageId': str(uuid.uuid4()),
                'payloadVersion': kwargs.get('payload_version', '3')
            },
            'endpoint': {
                "scope": {
                    "type": "BearerToken",
                    "token": kwargs.get('token', 'INVALID')
                },
                "endpointId": kwargs.get('endpoint_id', 'INVALID')
            },
            'payload': kwargs.get('payload', {})
        }
        if 'correlation_token' in kwargs:
            self.event['header']['correlation_token'] = kwargs.get('correlation_token', 'INVALID')
        if 'cookie' in kwargs:
            self.event['endpoint']['cookie'] = kwargs.get('cookie', '{}')
        if self.event['header']['name'] == 'AcceptGrant.Response' \
                or self.event['header']['name'] == 'Discover.Response':
            self.event.pop('endpoint')

    def add_context_property(self, **kwargs):
        self.context_properties.append(self.create_context_property(**kwargs))

    def add_cookie(self, key, value):
        self.cookies[key] = value

    def add_payload_endpoint(self, **kwargs):
        self.payload_endpoints.append(self.create_payload_endpoint(**kwargs))

    @staticmethod
    def get_utc_timestamp(seconds=None):
        return time.strftime('%Y-%m-%dT%H:%M:%S.00Z', time.gmtime(seconds))

    @classmethod
    def create_context_property(cls, **kwargs):
        return {
            'namespace': kwargs.get('namespace', 'Alexa.EndpointHealth'),
            'name': kwargs.get('name', 'connectivity'),
            'value': kwargs.get('value', {'value': 'OK'}),
            'timeOfSample': cls.get_utc_timestamp(kwargs.get('timestamp', None)),
            'uncertaintyInMilliseconds': kwargs.get('uncertainty_in_milliseconds', 0)
        }

    @staticmethod
    def create_payload_endpoint(**kwargs):
        return {
            'capabilities': kwargs.get('capabilities', []),
            'description': kwargs.get('description', 'Sample Endpoint Description'),
            'displayCategories': kwargs.get('display_categories', ['OTHER']),
            'endpointId': kwargs.get('endpoint_id', str(uuid.uuid4())),
            'friendlyName': kwargs.get('friendly_name', 'Sample Endpoint'),
            'manufacturerName': kwargs.get('manufacturer_name', 'Sample Manufacturer'),
            'cookie': kwargs.get('cookie', {})
        }

    @staticmethod
    def create_payload_endpoint_capability(**kwargs):
        capability = {
            'type': kwargs.get('type', 'AlexaInterface'),
            'interface': kwargs.get('interface', 'Alexa'),
            'instance': kwargs.get('instance'),
            'version': kwargs.get('version', '3'),
        }
        if supported := kwargs.get('supported', None):
            capability['properties'] = {}
            capability['properties']['supported'] = supported
            capability['properties']['proactivelyReported'] = kwargs.get('proactively_reported', False)
            capability['properties']['retrievable'] = kwargs.get('retrievable', False)
            capability['properties']['nonControllable'] = kwargs.get('non_controllable', False)
        if configuration := kwargs.get('configuration', None):
            capability['configuration'] = configuration
        return capability

    def get(self, remove_empty=True):
        response = {
            'context': self.context,
            'event': self.event
        }
        if len(self.context_properties) > 0:
            response['context']['properties'] = self.context_properties
        if len(self.payload_endpoints) > 0:
            response['event']['payload']['endpoints'] = self.payload_endpoints
        if remove_empty:
            if len(response['context']) < 1:
                response.pop('context')
        return response

    def set_payload(self, payload):
        self.event['payload'] = payload

    def set_payload_endpoint(self, payload_endpoints):
        self.payload_endpoints = payload_endpoints

    def set_payload_endpoints(self, payload_endpoints):
        if 'endpoints' not in self.event['payload']:
            self.event['payload']['endpoints'] = []
        self.event['payload']['endpoints'] = payload_endpoints
