import secrets
from pony.orm import *
from application.models.db import db
from authlib.oauth2.rfc6749 import ClientMixin, TokenMixin, AuthorizationCodeMixin
from datetime import datetime


class OauthClient(db.Entity, ClientMixin):
    uid = Required(str, unique=True)
    secret = Required(str, default=lambda: secrets.token_urlsafe())
    name = Required(str)
    tokens = Set(lambda: OauthToken)
    auth_codes = Set(lambda: OauthCode)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_client_id(self) -> int:
        return self.uid

    def get_default_redirect_uri(self) -> str:
        return str()

    def get_allowed_scope(self, scope: str) -> str:
        return str()

    def check_redirect_uri(self, redirect_uri: str) -> bool:
        return True

    def has_client_secret(self) -> bool:
        return bool(self.secret)

    def check_client_secret(self, client_secret: str) -> bool:
        return client_secret == self.secret

    def check_token_endpoint_auth_method(self, method) -> bool:
        return True

    def check_response_type(self, response_type) -> bool:
        return True

    def check_grant_type(self, grant_type) -> bool:
        return True


class OauthToken(db.Entity, TokenMixin):
    access_token = Required(str)
    token_type = Required(str)
    created_at = Required(datetime, default=lambda: datetime.now())
    expires_in = Required(datetime)
    scope = Optional(str, default=str())

    user = Required('User')
    client = Required(OauthClient)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_client_id(self) -> int:
        return self.user.id

    def get_scope(self) -> str:
        return self.scope

    def get_expires_in(self) -> int:
        return int(self.expires_in.timestamp())

    def get_expires_at(self) -> int:
        return int(self.created_at.timestamp()) + self.get_expires_in()


class OauthCode(db.Entity, AuthorizationCodeMixin):
    code = Required(str)
    redirect_uri = Required(str)
    client = Required(OauthClient)
    user = Required('User')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @staticmethod
    def is_expired() -> bool:
        return False

    def get_redirect_uri(self) -> str:
        return self.redirect_uri

    def get_scope(self) -> str:
        return str()
