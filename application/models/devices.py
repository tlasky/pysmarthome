import uuid
import logging
from typing import *
from pony.orm import *
from datetime import datetime, timedelta, timezone
from application.models.db import db
from application.traits import MQTTDeviceTraitHandler, AmazonDeviceTraitHandler, GoogleDeviceTraitHandler, \
    trait_controller
from application.create import mqtt_listener
from config import DEVICE_RESPONSE_LIMIT


class UsersDevice(db.Entity):
    uid = Required(uuid.UUID, default=uuid.uuid4)

    amazon_type = Optional(StrArray, default=['OTHER'])
    google_type = Optional(str)

    custom_data = Optional(Json, default=dict())

    name = Required(str)
    description = Required(str, default='PySmartHome device.')

    traits: Iterable['UsersDeviceTrait'] = Set('UsersDeviceTrait')
    user = Required('User')

    @property
    def online(self) -> bool:
        """
        If last update was at least DEVICE_RESPONSE_LIMIT seconds ago, device is online.
        """
        for trait in self.traits:
            if datetime.now() - trait.updated_at < timedelta(seconds=DEVICE_RESPONSE_LIMIT):
                return True
        return False

    def to_dict(self, *args, **kwargs) -> dict:
        """
        Rewrite to_dict because @property is used to set handlers
        """
        device_dict = super().to_dict(*args, **kwargs)
        device_dict['online'] = self.online
        return device_dict


class UsersDeviceTrait(db.Entity):
    value = Optional(Json, volatile=True)
    updated_at = Required(datetime, default=lambda: datetime.now(timezone.utc), volatile=True)

    mqtt_handler_name = Optional(str)
    amazon_handler_name = Optional(str)
    google_handler_name = Optional(str)

    readable = Required(bool)
    settable = Required(bool)

    mqtt_get = Optional(str)
    mqtt_set = Optional(str)

    custom_data = Optional(Json, default=dict())

    device = Required(UsersDevice)

    def get_value(self) -> Any:
        """
        Value getter
        """
        if self.value:
            return self.value
        elif self.mqtt_handler:
            return self.mqtt_handler.default_value
        return None

    def update(self):
        self.updated_at = datetime.now(timezone.utc)

    def send_value(self, value: Any):
        if self.mqtt_handler and self.mqtt_set:
            mqtt_listener.mqtt.publish(
                self.mqtt_set,
                self.mqtt_handler.dump_value(value)
            )

    def set_update_send_value(self, value: Any):
        """
        MQTT Trigger
        Value of trait should be set only using this!
        """
        self.value = value
        self.update()
        self.send_value(value)

    def to_dict(self, *args, **kwargs) -> dict:
        """
        Rewrite to_dict because @property is used to set handlers
        """
        trait_dict = super().to_dict(*args, **kwargs)
        for key in ['mqtt_handler', 'amazon_handler', 'google_handler']:
            if trait_dict.get(f'{key}_name'):
                trait_dict[key] = trait_dict.pop(f'{key}_name')
        return trait_dict

    @property
    def mqtt_handler(self) -> Union[MQTTDeviceTraitHandler, None]:
        """
        Handler getter
        """
        if handler := trait_controller.get_mqtt_handler(self.mqtt_handler_name):
            return handler(self)
        return None

    @mqtt_handler.setter
    def mqtt_handler(self, value: Union[str, Type[MQTTDeviceTraitHandler]]):
        """
        Safe handler setter
        """
        if issubclass(type(value), MQTTDeviceTraitHandler):
            value = value.name
        handler = trait_controller.get_mqtt_handler(value)
        if value and not handler:
            raise ValueError(f'Unsupported MQTT handler "{value}".')
        if self.amazon_handler and type(self.amazon_handler) not in trait_controller.get_compatible(handler):
            raise ValueError(f'MQTT handler is not compatible with specified Amazon handler.')
        if self.google_handler and type(self.google_handler) not in trait_controller.get_compatible(handler):
            raise ValueError(f'MQTT handler is not compatible with specified Google handler.')
        if not value:
            self.value = None
            self.amazon_handler = None
            self.google_handler = None
        self.mqtt_handler_name = value
        if self.mqtt_handler:
            self.value = self.mqtt_handler.default_value
            self.update()

    @property
    def amazon_handler(self) -> Union[AmazonDeviceTraitHandler, None]:
        """
        Handler getter
        """
        if handler := trait_controller.get_amazon_handler(self.amazon_handler_name):
            return handler(self)
        return None

    @amazon_handler.setter
    def amazon_handler(self, value: Union[str, Type[AmazonDeviceTraitHandler]]):
        """
        Safe handler setter
        """
        if value:
            if issubclass(type(value), AmazonDeviceTraitHandler):
                value = value.name
            if not (handler := trait_controller.get_amazon_handler(value)):
                raise ValueError(f'Unsupported Amazon handler "{value}".')
            if not self.mqtt_handler:
                raise ValueError('Please set MQTT handler firs.')
            if type(self.mqtt_handler) not in trait_controller.get_compatible(handler):
                raise ValueError('This Amazon handler is not compatible with specified MQTT handler.')
            if not handler.multiple and UsersDeviceTrait.select(
                    lambda t:
                    t.device == self.device and
                    t.amazon_handler_name == value and
                    t != self
            ).first():
                raise ValueError(f'Device cannot have multiple of "{value}".')
        self.amazon_handler_name = value

    @property
    def google_handler(self) -> Union[GoogleDeviceTraitHandler, None]:
        """
        Handler getter
        """
        if handler := trait_controller.get_google_handler(self.google_handler_name):
            return handler(self)
        return None

    @google_handler.setter
    def google_handler(self, value: Union[str, Type[GoogleDeviceTraitHandler]]):
        """
        Safe handler setter
        """
        if value:
            if issubclass(type(value), GoogleDeviceTraitHandler):
                value = value.name
            if not (handler := trait_controller.get_google_handler(value)):
                raise ValueError(f'Unsupported Google handler "{value}".')
            if not self.mqtt_handler:
                raise ValueError('Please set MQTT handler firs.')
            if type(self.mqtt_handler) not in trait_controller.get_compatible(handler):
                raise ValueError('This Google handler is not compatible with specified MQTT handler.')
            if not handler.multiple and UsersDeviceTrait.select(
                    lambda t:
                    t.device == self.device and
                    t.google_handler_name == value and
                    t != self
            ).first():
                raise ValueError(f'Device cannot have multiple of "{value}".')
        self.google_handler_name = value

    @staticmethod
    def get_mqtt_topics() -> List[str]:
        """
        MQTT Listener get_topics
        """
        with db_session(optimistic=False):
            return [
                trait.mqtt_get
                for trait in UsersDeviceTrait.select()
                if trait.mqtt_get
            ]

    @staticmethod
    def on_mqtt_message(topic: str, data: str):
        """
        MQTT Listener on_message
        """
        with db_session(optimistic=False):
            for trait in UsersDeviceTrait.select(mqtt_get=topic):
                trait: UsersDeviceTrait
                if trait.mqtt_handler:
                    try:
                        trait.mqtt_handler.set_trait_value(data)
                    except Exception as exc:
                        logging.error(f'Cannot set value of "{topic}" to {data}. ({exc})')
            db.commit()
