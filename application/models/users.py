from pony.orm import *
from datetime import datetime
from application.models.db import db
from flask_security import UserMixin, RoleMixin, PonyUserDatastore


class User(db.Entity, UserMixin):
    email = Required(str, unique=True)
    password = Required(str)
    active = Required(bool, default=False)
    confirmed_at = Optional(datetime)
    current_login_at = Optional(datetime)
    current_login_ip = Optional(str)
    login_count = Required(int, default=0)

    fs_uniquifier = Optional(str, unique=True)
    fs_token_uniquifier = Optional(str, unique=True)

    roles = Set('Role')
    oauth_codes = Set('OauthCode')
    oauth_tokens = Set('OauthToken')
    devices = Set('UsersDevice')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get_user_id(self) -> int:
        """
        Oauth user id getter
        """
        return self.id


class Role(db.Entity, RoleMixin):
    name = Required(str, unique=True)
    users = Set(User)


security_data_store = PonyUserDatastore(
    db,
    User, Role
)
