from application.models.db import db
from application.models.users import *
from application.models.oauth import *
from application.models.devices import *
from application.models.alexa_response import AlexaResponse


db.generate_mapping(create_tables=True)
