import uuid
import logging
from typing import *
from paho.mqtt import client
from threading import Thread


class MQTTListener:
    def __init__(self, host: str, port: str):
        self.host, self.port = host, port
        self.mqtt = client.Client(f'PySmartHome-{uuid.uuid4()}')
        self.mqtt.on_message = lambda cl, ud, message: self.on_message(message.topic, message.payload.decode())
        self.mqtt.on_connect = lambda *args: print('MQTT connected')
        self.mqtt.on_disconnect = lambda *args: print('MQTT disconnected')

        self.mqtt.connect(self.host, self.port)

        self.subscribed: Set[str] = set()
        self.is_running = False
        self.thread = Thread(target=self._worker)

    def enable_debug_logging(self):
        """
        Enable debugging output
        """
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        logger.addHandler(ch)
        self.mqtt.enable_logger(logger)

    def disable_debug_logging(self):
        """
        Disable debugging output
        """
        self.mqtt.disable_logger()

    @staticmethod
    def get_topics() -> List[str]:
        """
        Get list of topics to listen
        """
        raise NotImplementedError()

    @staticmethod
    def on_message(topic: str, data: str):
        """
        On message handler
        """
        raise NotImplementedError()

    def loop(self):
        """
        Listener single loop
        """
        topics = self.get_topics()
        to_subscribe = [
            topic
            for topic in topics
            if topic not in self.subscribed
        ]
        to_unsubscribe = [
            topic
            for topic in self.subscribed
            if topic not in topics
        ]
        for topic in to_subscribe:
            self.mqtt.subscribe(topic)
            self.subscribed.add(topic)
        for topic in to_unsubscribe:
            self.mqtt.unsubscribe(topic)
            if topic in self.subscribed:
                self.subscribed.remove(topic)
        self.mqtt.loop()

    def loop_forever(self):
        """
        Run standalone
        """
        try:
            while True:
                self.loop()
        except KeyboardInterrupt:
            pass
        finally:
            self.stop()

    def _worker(self):
        """
        Run with webapp - worker
        """
        while self.is_running:
            self.loop()

    def start(self):
        """
        Run with webapp
        """
        self.is_running = True
        self.thread.start()

    def stop(self):
        """
        Stop with webapp
        """
        if self.is_running:
            self.is_running = False
            self.mqtt.disconnect()
            self.thread.join()
