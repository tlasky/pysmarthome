import humanize
from flask import Flask
from flask_mail import Mail
from flask_babelex import Babel
from flask_security import Security
from flask_bootstrap import Bootstrap
from pony.flask import Pony
from flask_restx import Api
from config import static_path, templates_path
from werkzeug.middleware.proxy_fix import ProxyFix
from authlib.integrations.flask_oauth2 import AuthorizationServer
from application.mqtt_listener import MQTTListener
from application.utils import CustomJSONEncoder

app = Flask(
    __name__,
    static_folder=static_path,
    template_folder=templates_path,
    static_url_path=''
)
app.config.from_object('config')
app.wsgi_app = ProxyFix(app.wsgi_app)
app.json_encoder = CustomJSONEncoder

Pony(app)
mail = Mail(app)
babel = Babel(app)
security = Security(app)
oauth_handler = AuthorizationServer()
Bootstrap(app)
api = Api(
    app=app,
    prefix='/api/v1',
    doc='/api',
    version='1.0',
    title='PySmartHome API',
    description='PySmartHome API',
)

mqtt_listener = MQTTListener(
    app.config['MQTT_HOST'],
    app.config['MQTT_PORT']
)


@app.context_processor
def context_processor():
    return dict(
        humanize=humanize
    )
