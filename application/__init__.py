from application.create import *
from application.models import *
from application.traits import *
from application.oauth import *
from application.forms import *
from application.views import *
from application.api import *

from flask_security import hash_password

# Setting security
security.init_app(
    app=app,
    datastore=security_data_store,
    register_form=ExtendedRegisterForm
)

# Setting OAuth provider
oauth_handler.init_app(
    app=app,
    query_client=query_client,
    save_token=save_token
)
oauth_handler.register_grant(grants.ImplicitGrant)
oauth_handler.register_grant(grants.ClientCredentialsGrant)
oauth_handler.register_grant(AuthorizationCodeGrant)

# Setting MQTT Listener
mqtt_listener.get_topics = UsersDeviceTrait.get_mqtt_topics
mqtt_listener.on_message = UsersDeviceTrait.on_mqtt_message


@app.before_first_request
def initialize_app():
    with db_session:
        if not security_data_store.find_user(email='testuser@example.com'):
            print('Creating test user.')
            user = security_data_store.create_user(
                email='testuser@example.com',
                password=hash_password('123*'),
                confirmed_at=datetime.now()
            )
            security_data_store.activate_user(user)
            db.commit()

        if not OauthClient.get(id=1):
            print('Creating Amazon oauth.')
            OauthClient(
                uid='amazon',
                secret='amazon_secret',  # Just a testing value
                name='Amazon'
            )
            db.commit()

        if not OauthClient.get(id=2):
            print('Creating Google oauth.')
            OauthClient(
                uid='google',
                secret='google_secret',  # Just a testing value
                name='Google'
            )
            db.commit()
