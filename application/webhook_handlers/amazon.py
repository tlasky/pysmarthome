from authlib.integrations.flask_oauth2 import current_token
from application.models import *


def device_offline_response(request_dict: dict) -> AlexaResponse:
    return AlexaResponse(
        name='ErrorResponse',
        payload={
            'type': 'ENDPOINT_UNREACHABLE',
            'message': 'Device is offline.'
        },
        endpoint_id=request_dict['directive']['endpoint']['endpointId'],
        token=current_token.access_token
    )


def handle_discovery() -> dict:
    response = AlexaResponse(
        namespace='Alexa.Discovery',
        name='Discover.Response',
        token=current_token.access_token
    )
    for device in current_token.user.devices:
        device: UsersDevice
        if device.amazon_type:
            capabilities = [response.create_payload_endpoint_capability()]
            for trait in device.traits:
                if trait.amazon_handler:
                    capabilities.append(response.create_payload_endpoint_capability(
                        interface=trait.amazon_handler.name,
                        supported=trait.amazon_handler.get_supported_properties(),
                        proactively_reported=True,
                        retrievable=trait.readable,
                        non_controllable=not trait.settable,
                        configuration=trait.amazon_handler.get_configuration()
                    ))
            response.add_payload_endpoint(
                endpoint_id=str(device.uid),
                friendly_name=device.name,
                description=device.description,
                capabilities=capabilities,
                manufacturer_name='PySmartHome',
                display_categories=device.amazon_type,
                cookie=device.custom_data
            )
    return response.get()


def handle_report(request_dict: dict) -> dict:
    device: UsersDevice = UsersDevice.get(
        user=current_token.user,
        uid=request_dict['directive']['endpoint']['endpointId']
    )
    if device and device.online:
        response = AlexaResponse(
            name='StateReport',
            correlation_token=request_dict['directive']['header']['correlationToken'],
            endpoint_id=str(device.uid),
            token=current_token.access_token
        )
        for trait in device.traits:
            if trait.amazon_handler:
                for trait_value in trait.amazon_handler.get_trait_value():
                    response.add_context_property(
                        namespace=trait.amazon_handler.name,
                        timestamp=trait.updated_at.timestamp(),
                        **trait_value
                    )
    else:
        response = device_offline_response(request_dict)
    return response.get()


def handle_command(request_dict: dict) -> dict:
    device: UsersDevice = UsersDevice.get(
        user=current_token.user,
        uid=request_dict['directive']['endpoint']['endpointId']
    )
    if device and device.online:
        response = AlexaResponse(
            correlation_token=request_dict['directive']['header']['correlationToken'],
            endpoint_id=str(device.uid),
            token=current_token.access_token
        )
        for trait in device.traits:
            if trait.amazon_handler \
                    and trait.amazon_handler.name == request_dict['directive']['header']['namespace'] \
                    and trait.amazon_handler.can_handle_command(request_dict['directive']['header']['name']):
                trait.amazon_handler.handle_command(
                    request_dict['directive']['header']['name'],
                    request_dict['directive']['payload']
                )
                for trait_value in trait.amazon_handler.get_trait_value():
                    response.add_context_property(
                        namespace=trait.amazon_handler.name,
                        timestamp=trait.updated_at.timestamp(),
                        **trait_value
                    )
    else:
        response = device_offline_response(request_dict)
    return response.get()


def handle(request_dict: dict) -> dict:
    name = request_dict['directive']['header']['name']
    namespace = request_dict['directive']['header']['namespace']

    if namespace == 'Alexa.Discovery':
        if name == 'Discover':
            return handle_discovery()

    if namespace == 'Alexa':
        if name == 'ReportState':
            return handle_report(request_dict)

    for handler in trait_controller.amazon_handlers:
        if namespace == handler.name:
            return handle_command(request_dict)

    return AlexaResponse(
        name='ErrorResponse',
        payload={
            'type': 'INTERNAL_ERROR',
            'message': f'Unsupported name {name} in namespace {namespace}.'
        }
    ).get()
