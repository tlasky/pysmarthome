from authlib.integrations.flask_oauth2 import current_token
from application.models import *


def handle_sync(request_dict: dict) -> dict:
    devices_dicts = list()
    for device in current_token.user.devices:
        device: UsersDevice
        traits = [
            trait.google_handler.name
            for trait in device.traits
            if trait.google_handler
        ]
        attributes = dict()
        for trait in device.traits:
            if trait.google_handler:
                attributes.update(trait.google_handler.get_attributes())
        if device.google_type:
            devices_dicts.append({
                "id": str(device.uid),
                "type": device.google_type,
                "traits": traits,
                "name": {
                    "name": device.name
                },
                "attributes": attributes,
                "willReportState": False,
                "customData": device.custom_data
            })
    return {
        'requestId': request_dict['requestId'],
        'payload': {
            'agentUserId': str(current_token.user.id),
            'devices': devices_dicts
        }
    }


def handle_query(request_dict: dict) -> dict:
    devices_dict = dict()
    for requested_device in request_dict['inputs'][0]['payload']['devices']:
        device_dict = {
            'status': 'ERROR'
        }
        device: UsersDevice = UsersDevice.get(
            user=current_token.user,
            uid=requested_device['id']
        )
        if device:
            device_dict.update({
                'online': device.online,
                'status': 'SUCCESS'
            })
            for trait in device.traits:
                if trait.google_handler:
                    device_dict.update(trait.google_handler.get_trait_value())
        devices_dict[str(device.uid)] = device_dict
    return {
        'requestId': request_dict['requestId'],
        'payload': {
            'devices': devices_dict
        }
    }


def handle_execute(request_dict: dict) -> dict:
    online_devices_ids = list()
    online_devices_states = {'online': True}
    offline_devices_ids = list()
    for requested_command in request_dict['inputs'][0]['payload']['commands']:
        for requested_device in requested_command['devices']:
            device: UsersDevice = UsersDevice.get(user=current_token.user, uid=requested_device['id'])
            if device and device.online:
                online_devices_ids.append(str(device.uid))
                for trait in device.traits:
                    if trait.google_handler:
                        for execution_request in requested_command['execution']:
                            online_devices_states.update(execution_request['params'])
                            if trait.google_handler.can_handle_command(execution_request['command']):
                                trait.google_handler.handle_command(
                                    execution_request['command'],
                                    execution_request['params']
                                )
            else:
                offline_devices_ids.append(requested_device['id'])
    db.commit()
    commands = []
    if online_devices_ids:
        commands.append({
            "ids": online_devices_ids,
            "status": "SUCCESS",
            "states": online_devices_states
        })
    if offline_devices_ids:
        commands.append({
            "ids": offline_devices_ids,
            "status": "ERROR",
            "errorCode": "deviceTurnedOff"
        })
    return {
        'requestId': request_dict['requestId'],
        'payload': {
            "commands": commands
        }
    }


def handle(request_dict: dict) -> dict:
    for request_input in request_dict['inputs']:
        intent = request_input['intent']

        if intent == 'action.devices.SYNC':
            return handle_sync(request_dict)

        elif intent == 'action.devices.QUERY':
            return handle_query(request_dict)

        elif intent == 'action.devices.EXECUTE':
            return handle_execute(request_dict)

    return dict()
