from typing import *
from application.traits.base import GoogleDeviceTraitHandler
from application.traits.mqtt import *
from application.utils import correct_range
from colorutils import rgb_to_hsv, hex_to_hsv, hsv_to_hex, hsv_to_rgb


class GoogleOnOffTraitHandler(GoogleDeviceTraitHandler):
    name = 'action.devices.traits.OnOff'
    commands = [
        'action.devices.commands.OnOff'
    ]
    compatible = {
        MQTTBoolTraitHandler
    }
    description = """
    Turn on/off device.
    """

    def get_trait_value(self):
        return {
            'on': self.trait.get_value(),
            "commandOnlyOnOff": self.trait.settable and self.trait.readable,
            "queryOnlyOnOff": self.trait.readable and not self.trait.settable
        }

    def handle_command(self, command: str, payload: dict):
        self.trait.set_update_send_value(payload['on'])


class GoogleBrightnessTraitHandler(GoogleDeviceTraitHandler):
    name = 'action.devices.traits.Brightness'
    commands = [
        'action.devices.commands.BrightnessAbsolute',
        'action.devices.commands.BrightnessRelative'
    ]
    compatible = {
        MQTTIntTraitHandler
    }
    description = """
    Set device brightness. (0 to 100%) 
    """

    def get_trait_value(self):
        return {
            'brightness': int(correct_range(self.trait.get_value(), 0, 100)),
            "commandOnlyBrightness": self.trait.settable and self.trait.readable,
        }

    def handle_command(self, command: str, payload: dict):
        if command == self.commands[0]:
            self.trait.set_update_send_value(payload['brightness'])
        elif command == self.commands[1]:
            value = self.trait.get_value() + (
                    payload.get('brightnessRelativePercent', 0) or payload.get('brightnessRelativeWeight', 0)
            )
            self.trait.set_update_send_value(int(correct_range(value, 0, 100)))


class GoogleColorSettingTraitHandler(GoogleDeviceTraitHandler):
    name = 'action.devices.traits.ColorSetting'
    commands = [
        'action.devices.commands.ColorAbsolute',
    ]
    compatible = {
        MQTTHSVColorTraitHandler,
        MQTTRGBColorTraitHandler,
        MQTTHEXColorTraitHandler
    }
    description = """
    Set device color.
    """

    def get_attributes(self) -> Dict[str, str]:
        return {
            "colorModel": "hsv",
            "commandOnlyColorSetting": self.trait.settable and self.trait.readable,
        }

    def get_trait_value(self):
        h, s, v = Color(web='white').hsv
        if type(self.trait.mqtt_handler) is MQTTRGBColorTraitHandler:  # Match right MQTT handler
            h, s, v = rgb_to_hsv(self.trait.get_value())
        elif type(self.trait.mqtt_handler) is MQTTHSVColorTraitHandler:
            h, s, v = self.trait.get_value()
        elif type(self.trait.mqtt_handler) is MQTTHEXColorTraitHandler:
            h, s, v = hex_to_hsv(self.trait.get_value())
        return {
            'color': {
                'spectrumHsv': {
                    'hue': h,
                    'saturation': s,
                    'value': v
                }
            }
        }

    def handle_command(self, command: str, payload: dict):
        hsv = [
            payload['color']['spectrumHSV']['hue'],
            payload['color']['spectrumHSV']['saturation'],
            payload['color']['spectrumHSV']['value']
        ]
        if type(self.trait.mqtt_handler) is MQTTRGBColorTraitHandler:  # Match right MQTT handler
            self.trait.set_update_send_value(list(map(int, hsv_to_rgb(hsv))))
        elif type(self.trait.mqtt_handler) is MQTTHSVColorTraitHandler:
            self.trait.set_update_send_value(hsv)
        elif type(self.trait.mqtt_handler) is MQTTHEXColorTraitHandler:
            self.trait.set_update_send_value(hsv_to_hex(hsv))


class GoogleTemperatureControlTraitHandler(GoogleDeviceTraitHandler):
    name = 'action.devices.traits.TemperatureControl'
    commands = [
        'action.devices.commands.SetTemperature'
    ]
    min_threshold, max_threshold = -260, 260
    compatible = {
        MQTTFloatTraitHandler
    }
    description = f"""
    Temperature control.
    Units: Celsius
    You can set temperature range by setting min and max in custom data.
    Default is: {min_threshold} and {max_threshold}.
    """
    key_map = {
        'minThresholdCelsius': 'min',
        'maxThresholdCelsius': 'max',
        'temperatureSetpointCelsius': 'target'
    }

    def get_attributes(self) -> Dict[str, str]:
        return {
            "temperatureRange": {
                "minThresholdCelsius": self.trait.custom_data.get(
                    'min',
                    self.min_threshold
                ),
                "maxThresholdCelsius": self.trait.custom_data.get(
                    'max',
                    self.max_threshold
                )
            },
            "temperatureStepCelsius": 5,
            "temperatureUnitForUX": "C",
            "commandOnlyTemperatureControl": self.trait.settable and self.trait.readable,
            "queryOnlyTemperatureControl": self.trait.readable and not self.trait.settable
        }

    def get_trait_value(self):
        return {
            "temperatureSetpointCelsius": self.trait.custom_data.get('target'),
            "temperatureAmbientCelsius": self.trait.get_value()
        }

    def handle_command(self, command: str, payload: dict):
        for key, value in payload.items():
            if key in self.key_map.keys():
                key = self.key_map[key]
            self.trait.custom_data[key] = value
        if value := payload.get('temperatureSetpointCelsius'):
            self.trait.send_value(value)
            self.trait.update()


class GoogleTemperatureSettingTraitHandler(GoogleDeviceTraitHandler):
    name = 'action.devices.traits.TemperatureSetting'
    commands = [
        'action.devices.commands.ThermostatTemperatureSetpoint',
        'action.devices.commands.ThermostatTemperatureSetRange',
        'action.devices.commands.ThermostatSetMode',

    ]
    modes = ["off", "heat", "cool", "on", "auto", "eco"]
    compatible = {
        MQTTThermostatTraitHandler
    }
    description = f"""
    Temperature setting.
    Units: Celsius
    Modes: {', '.join(modes)}
    """
    key_map = {
        'activeThermostatMode': 'mode',
        'thermostatMode': 'mode',
        'thermostatTemperatureSetpointHigh': 'max',
        'thermostatTemperatureSetpointLow': 'min',
        'thermostatTemperatureAmbient': 'current'
    }

    def get_attributes(self) -> Dict[str, str]:
        return {
            "availableThermostatModes": self.trait.custom_data.get(
                'modes',
                self.modes
            ),
            "thermostatTemperatureUnit": "C",
            "commandOnlyTemperatureSetting": self.trait.settable and self.trait.readable,
            "queryOnlyTemperatureSetting": self.trait.readable and not self.trait.settable
        }

    def get_trait_value(self):
        value = self.trait.get_value()
        return {
            "activeThermostatMode": value['mode'],
            "thermostatMode": value['mode'],
            "thermostatTemperatureSetpointHigh": value['max'],
            "thermostatTemperatureSetpointLow": value['min'],
            "thermostatTemperatureAmbient": value['current']
        }

    def handle_command(self, command: str, payload: dict):
        value = self.trait.get_value()
        for key, new_value in payload.items():
            value[self.key_map[key]] = new_value
        value['target'] = (value['max'] - value['min']) / 2
        assert value['mode'] in self.trait.custom_data.get('modes', self.modes)
        self.trait.set_update_send_value(value)


class GoogleHumiditySettingTraitHandler(GoogleDeviceTraitHandler):
    name = 'action.devices.traits.HumiditySetting'
    commands = [
        'action.devices.commands.SetHumidity',
        'action.devices.commands.HumidityRelative'
    ]
    min_percent, max_percent = 0, 100
    compatible = {
        MQTTIntTraitHandler
    }
    description = f"""
    Setting and getting humidity.
    You can set min and max in custom data.
    """
    key_map = {
        'minPercent': 'min',
        'maxPercent': 'max',
        'humiditySetpointPercent': 'target'
    }

    def get_attributes(self):
        return {
            "humiditySetpointRange": {
                "minPercent": self.trait.custom_data.get(
                    'min',
                    self.min_percent
                ),
                "maxPercent": self.trait.custom_data.get(
                    'max',
                    self.max_percent
                )
            },
            "commandOnlyHumiditySetting": self.trait.settable and self.trait.readable,
            "queryOnlyHumiditySetting": self.trait.readable and not self.trait.settable
        }

    def get_trait_value(self):
        return {
            "humiditySetpointPercent": self.trait.custom_data.get('target'),
            "humidityAmbientPercent": int(correct_range(
                self.trait.get_value(),
                self.trait.custom_data.get(
                    'min',
                    self.min_percent
                ),
                self.trait.custom_data.get(
                    'max',
                    self.max_percent
                )
            ))
        }

    def handle_command(self, command: str, payload: dict):
        value = self.trait.get_value()
        if command == self.commands[0]:
            value = payload['humidity']
        elif command == self.commands[1]:
            value = self.trait.custom_data.get('humiditySetpointPercent', value) + (
                    payload.get('humidityRelativePercent', 0) or payload.get('humidityRelativeWeight', 0)
            )
        self.trait.custom_data['target'] = correct_range(value, 0, 100)
        self.trait.send_value(int(correct_range(value, 0, 100)))
        self.trait.update()
