from application.traits.base import MQTTDeviceTraitHandler
from colorutils import Color, hsv_to_hex, rgb_to_hex


class MQTTBoolTraitHandler(MQTTDeviceTraitHandler):
    name = 'bool'
    default_value = bool()
    schema = {
        'type': 'boolean',
        'default': default_value
    }
    description = f"""
    Expected value is true or false.
    """

    def set_trait_value(self, value: str):
        self.trait.value = self.load_value(value)
        self.trait.update()


class MQTTFloatTraitHandler(MQTTDeviceTraitHandler):
    name = 'float'
    default_value = float()
    schema = {
        'type': 'number',
        'default': default_value
    }
    description = f"""
    Expected value is any floating point number.
    """

    def set_trait_value(self, value: str):
        self.trait.value = self.load_value(value)
        self.trait.update()


class MQTTIntTraitHandler(MQTTDeviceTraitHandler):
    name = 'int'
    default_value = int()
    schema = {
        'type': 'integer',
        'default': default_value
    }
    description = f"""
    Expected value is any integer.
    """

    def set_trait_value(self, value: str):
        self.trait.value = self.load_value(value)
        self.trait.update()


class MQTTStringTraitHandler(MQTTDeviceTraitHandler):
    name = 'string'
    default_value = str()
    schema = {
        'type': 'string',
        'default': default_value
    }
    description = f"""
    Expected value is any JSON string.
    """

    def set_trait_value(self, value: str):
        self.trait.value = self.load_value(value)
        self.trait.update()


class MQTTRGBColorTraitHandler(MQTTDeviceTraitHandler):
    name = 'rgb_color'
    default_value = [255, 255, 255]
    schema = {
        'type': 'array',
        'contains': {
            'type': 'integer',
            'minimum': 0,
            'maximum': 255
        },
        'maxItems': 3,
        'additionalItems': False,
        'default': default_value
    }
    description = f"""
    Array of three numbers where every value is integer between 0 and 255. [r, g, b]
    """

    def set_trait_value(self, value: str):
        self.trait.value = self.load_value(value)
        self.trait.update()

    def trait_value_html(self) -> str:
        return f"""
        <span class="badge text-dark" style="background-color: {rgb_to_hex(self.trait.get_value())};">
        {super().trait_value_html()}
        </span>
        """


class MQTTHEXColorTraitHandler(MQTTRGBColorTraitHandler):
    name = 'hex_color'
    default_value = Color(web='white').hex
    schema = {
        'type': 'string',
        'pattern': '^#(?:[0-9a-fA-F]{1,2}){3}$'
    }
    description = f"""
    Hex color.
    """

    def set_trait_value(self, value: str):
        self.trait.value = Color(hex=self.load_value(value)).hex
        self.trait.update()

    def trait_value_html(self) -> str:
        return f"""
        <span class="badge text-dark" style="background-color: {self.trait.get_value()};">
        {super().trait_value_html()}
        </span>
        """


class MQTTHSVColorTraitHandler(MQTTRGBColorTraitHandler):
    name = 'hsv_color'
    default_value = list(Color(web='white').hsv)
    schema = {
        'type': 'array',
        'items': [
            {
                'type': 'integer',
                'minimum': 0,
                'maximum': 359
            },
            {
                'type': 'number',
                'minimum': 0,
                'maximum': 1
            },
            {
                'type': 'number',
                'minimum': 0,
                'maximum': 1
            },
        ],
        'maxItems': 3,
        'additionalItems': False,
        'default': default_value
    }
    description = f"""
    Array of three numbers. [h, s, v]
    """

    def set_trait_value(self, value: str):
        self.trait.value = Color(hsv=self.load_value(value)).hsv
        self.trait.update()

    def trait_value_html(self) -> str:
        return f"""
        <span class="badge text-dark" style="background-color: {hsv_to_hex(self.trait.get_value())};">
        {super().trait_value_html()}
        </span>
        """


class MQTTThermostatTraitHandler(MQTTDeviceTraitHandler):
    name = 'thermostat'
    modes = ["off", "heat", "cool", "on", "auto", "eco"]
    default_value = dict(
        current=0,
        target=0,
        min=0,
        max=0,
        mode=modes[0]
    )
    schema = {
        'type': 'object',
        'properties': {
            'current': 'number',
            'target': 'number',
            'min': 'number',
            'max': 'number',
            'mode': 'string',
        },
        'required': list(default_value.keys())
    }
    description = f"""
    Thermostat setting.
    Default modes: {', '.join(modes)}
    """

    def set_trait_value(self, value: str):
        new_value = self.trait.get_value()
        new_value.update(self.load_value(value))
        assert new_value['mode'] in self.trait.custom_data.get('modes', self.modes)
        self.trait.value = new_value
        self.trait.update()