from application.traits.amazon import *
from application.traits.google import *
from application.traits.base import DeviceTraitHandler, AssistantDeviceTraitHandler


class TraitController:
    mqtt_handlers: Set[Type[MQTTDeviceTraitHandler]] = {  # Set of all MQTT handlers
        MQTTBoolTraitHandler,
        MQTTIntTraitHandler,
        MQTTFloatTraitHandler,
        MQTTStringTraitHandler,
        MQTTRGBColorTraitHandler,
        MQTTHSVColorTraitHandler,
        MQTTHEXColorTraitHandler,
        MQTTThermostatTraitHandler
    }
    amazon_handlers: Set[Type[AmazonDeviceTraitHandler]] = {  # Set of all Amazon handlers
        AmazonPowerControllerTraitHandler,
        AmazonBrightnessControllerTraitHandler,
        AmazonColorControllerTraitHandler,
        AmazonTemperatureSensorTraitHandler,
        AmazonPercentageControllerTraitHandler,
        AmazonThermostatControllerTraitHandler
    }
    google_handlers: Set[Type[GoogleDeviceTraitHandler]] = {  # Set of all Google handlers
        GoogleOnOffTraitHandler,
        GoogleBrightnessTraitHandler,
        GoogleColorSettingTraitHandler,
        GoogleTemperatureControlTraitHandler,
        GoogleHumiditySettingTraitHandler,
        GoogleTemperatureSettingTraitHandler,
    }

    def __init__(self):
        # Creating handlers map handler.name -> handler class
        self.mqtt_name_map: Dict[str, MQTTDeviceTraitHandler] = {
            handler.name: handler
            for handler in self.mqtt_handlers
        }
        self.amazon_name_map: Dict[str, AmazonDeviceTraitHandler] = {
            handler.name: handler
            for handler in self.amazon_handlers
        }
        self.google_name_map: Dict[str, GoogleDeviceTraitHandler] = {
            handler.name: handler
            for handler in self.google_handlers
        }

    def get_mqtt_handler(self, name: str) -> Union[Type[MQTTDeviceTraitHandler], None]:
        """
        Finding controllers by name or class name
        """
        return self.mqtt_name_map.get(name)

    def get_mqtt_handlers(self) -> List[Type[MQTTDeviceTraitHandler]]:
        """
        Get MQTT handlers (sorted)
        """
        return list(sorted(
            self.mqtt_handlers,
            key=lambda h: h.name
        ))

    def get_amazon_handler(self, name: str) -> Union[Type[AmazonDeviceTraitHandler], None]:
        """
        Finding controllers by name or class name
        """
        return self.amazon_name_map.get(name)

    def get_amazon_handlers(self) -> List[Type[AmazonDeviceTraitHandler]]:
        """
        Get Amazon handlers (sorted)
        """
        return list(sorted(
            self.amazon_handlers,
            key=lambda h: h.name
        ))

    def get_google_handler(self, name: str) -> Union[Type[GoogleDeviceTraitHandler], None]:
        """
        Finding controllers by name or class name
        """
        return self.google_name_map.get(name)

    def get_google_handlers(self) -> List[Type[GoogleDeviceTraitHandler]]:
        """
        Get Google handlers (sorted)
        """
        return list(sorted(
            self.google_handlers,
            key=lambda h: h.name
        ))

    def get_handler(self, name: str) -> Type[DeviceTraitHandler]:
        """
        Get any handler by name
        """
        return self.get_mqtt_handler(name) or self.get_amazon_handler(name) or self.get_google_handler(name)

    def get_compatible(self, handler: Type[DeviceTraitHandler]) -> List[Type[DeviceTraitHandler]]:
        """
        Find compatible handlers
        (And sort them by the name)
        """
        compatible = set()
        if issubclass(handler, MQTTDeviceTraitHandler):
            for assistant_handler in {*self.amazon_handlers, *self.google_handlers}:
                if handler in assistant_handler.compatible:
                    compatible.add(assistant_handler)
        elif issubclass(handler, AssistantDeviceTraitHandler):
            for compatible_handler in handler.compatible:
                compatible.add(compatible_handler)
                compatible.update(self.get_compatible(compatible_handler))
        if handler in compatible:
            compatible.remove(handler)
        return list(sorted(
            compatible,
            key=lambda h: h.name
        ))
