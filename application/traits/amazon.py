from typing import *
from application.traits.base import AmazonDeviceTraitHandler
from application.traits.mqtt import *
from application.utils import correct_range
from colorutils import rgb_to_hsv, hex_to_hsv, hsv_to_hex, hsv_to_rgb


class AmazonPowerControllerTraitHandler(AmazonDeviceTraitHandler):
    name = 'Alexa.PowerController'
    commands = [
        'TurnOn',
        'TurnOff'
    ]
    compatible = {
        MQTTBoolTraitHandler
    }
    description = """
    Turn on/off device.
    """

    def get_trait_value(self):
        return [
            {
                'name': 'powerState',
                'value': 'ON' if self.trait.get_value() else 'OFF'
            }
        ]

    @classmethod
    def get_supported_properties(cls) -> List[Dict[str, str]]:
        return [
            {'name': 'powerState'}
        ]

    def handle_command(self, command: str, payload: dict):
        if command == 'TurnOn':
            self.trait.set_update_send_value(True)
        elif command == 'TurnOff':
            self.trait.set_update_send_value(False)


class AmazonBrightnessControllerTraitHandler(AmazonDeviceTraitHandler):
    name = 'Alexa.BrightnessController'
    commands = [
        'SetBrightness',
        'AdjustBrightness'
    ]
    compatible = {
        MQTTIntTraitHandler
    }
    description = """
    Set device brightness. (0 to 100%) 
    """

    def get_trait_value(self):
        return [
            {
                'name': 'brightness',
                'value': int(correct_range(self.trait.get_value(), 0, 100))
            }
        ]

    @classmethod
    def get_supported_properties(cls) -> List[Dict[str, str]]:
        return [
            {'name': 'brightness'}
        ]

    def handle_command(self, command: str, payload: dict):
        if command == self.commands[0]:
            self.trait.set_update_send_value(payload['brightness'])
        elif command == self.commands[1]:
            value = self.trait.get_value() + payload['brightnessDelta']
            self.trait.set_update_send_value(int(correct_range(value, 0, 100)))


class AmazonColorControllerTraitHandler(AmazonDeviceTraitHandler):
    name = 'Alexa.ColorController'
    commands = ['SetColor']
    compatible = {
        MQTTRGBColorTraitHandler,
        MQTTHSVColorTraitHandler,
        MQTTHEXColorTraitHandler
    }
    description = """
    Set device color.
    """

    def get_trait_value(self):
        h, s, v = Color(web='white').hsv
        if type(self.trait.mqtt_handler) is MQTTRGBColorTraitHandler:
            h, s, v = rgb_to_hsv(self.trait.get_value())
        elif type(self.trait.mqtt_handler) is MQTTHSVColorTraitHandler:
            h, s, v = self.trait.get_value()
        elif type(self.trait.mqtt_handler) is MQTTHEXColorTraitHandler:
            h, s, v = hex_to_hsv(self.trait.get_value())
        return [
            {
                'name': 'color',
                'value': {
                    'hue': h,
                    'saturation': s,
                    'brightness': v
                }
            }
        ]

    @classmethod
    def get_supported_properties(cls) -> List[Dict[str, str]]:
        return [
            {'name': 'color'}
        ]

    def handle_command(self, command: str, payload: dict):
        hsv = [
            payload['color']['hue'],
            payload['color']['saturation'],
            payload['color']['brightness']
        ]
        if type(self.trait.mqtt_handler) is MQTTRGBColorTraitHandler:  # Match right MQTT handler
            self.trait.set_update_send_value(list(map(int, hsv_to_rgb(hsv))))
        elif type(self.trait.mqtt_handler) is MQTTHSVColorTraitHandler:
            self.trait.set_update_send_value(hsv)
        elif type(self.trait.mqtt_handler) is MQTTHEXColorTraitHandler:
            self.trait.set_update_send_value(hsv_to_hex(hsv))


class AmazonTemperatureSensorTraitHandler(AmazonDeviceTraitHandler):
    name = 'Alexa.TemperatureSensor'
    compatible = {
        MQTTFloatTraitHandler
    }
    description = """
    Temperature sensor.
    """

    def get_trait_value(self):
        return [
            {
                "name": "temperature",
                "value": {
                    "value": self.trait.get_value(),
                    "scale": "CELSIUS"
                }
            }
        ]

    @classmethod
    def get_supported_properties(cls) -> List[Dict[str, str]]:
        return [
            {'name': 'temperature'}
        ]

    def handle_command(self, command: str, payload: dict):
        pass


class AmazonPercentageControllerTraitHandler(AmazonDeviceTraitHandler):
    name = 'Alexa.PercentageController'
    commands = [
        'SetPercentage',
        'AdjustPercentage'
    ]
    compatible = {
        MQTTIntTraitHandler
    }
    default_min, default_max = 0, 100
    description = f"""
    Get or set percentage.
    You can set min and max in custom data.
    Default is {default_min} and {default_max}.
    """

    def get_trait_value(self):
        return [
            {
                'name': 'percentage',
                'value': int(correct_range(
                    self.trait.get_value(),
                    self.trait.custom_data.get(
                        'min',
                        self.default_min
                    ),
                    self.trait.custom_data.get(
                        'max',
                        self.default_max
                    )
                ))
            }
        ]

    @classmethod
    def get_supported_properties(cls) -> List[Dict[str, str]]:
        return [
            {"name": "percentage"}
        ]

    def handle_command(self, command: str, payload: dict):
        if command == self.commands[0]:
            self.trait.set_update_send_value(payload['percentage'])
        elif command == self.commands[1]:
            value = self.trait.get_value() + payload['percentageDelta']
            self.trait.set_update_send_value(int(correct_range(
                value,
                self.trait.custom_data.get(
                    'min',
                    self.default_min
                ),
                self.trait.custom_data.get(
                    'max',
                    self.default_max
                )
            )))


class AmazonThermostatControllerTraitHandler(AmazonDeviceTraitHandler):
    name = 'Alexa.ThermostatController'
    commands = [
        'SetTargetTemperature',
        'AdjustTargetTemperature',
        'SetThermostatMode',
        'ResumeSchedule'
    ]
    modes = ["off", "heat", "cool", "on", "auto", "eco"]
    compatible = {
        MQTTThermostatTraitHandler
    }
    description = f"""
    Temperature setting.
    Units: Celsius
    Modes: {', '.join(modes)}
    """
    key_map = {
        'targetSetpoint': 'target',
        'lowerSetpoint': 'min',
        'upperSetpoint': 'max',
        'thermostatMode': 'mode'
    }

    def get_trait_value(self):
        value = self.trait.get_value()
        return [
            {
                'name': amazon_key,
                'value': value[key].upper() if key == 'mode' else value[key]
            }
            for amazon_key, key in self.key_map.items()
        ]

    def get_configuration(self) -> Dict[str, Any]:
        return {
            'supportedModes': [
                mode.upper()
                for mode in self.trait.custom_data.get('modes', self.modes)
            ],
            'supportsScheduling': False
        }

    def get_supported_properties(self) -> List[Dict[str, str]]:
        return [
            {'name': key}
            for key in self.key_map.keys()
        ]

    def handle_command(self, command: str, payload: dict):
        value = self.trait.get_value()
        for amazon_key, key in self.key_map.items():
            if data := payload.get(amazon_key):
                if key == 'mode':
                    value[key] = data['value'].lower()
                else:
                    scale = data['scale']
                    if scale == 'CELSIUS':
                        value[key] = data['value']
                    elif scale == 'FAHRENHEIT':
                        value[key] = (data['value'] - 32) * 5 / 9
        assert value['mode'] in self.trait.custom_data.get('modes', self.modes)
        self.trait.set_update_send_value(value)
