import json
import jsonschema
from abc import ABC
from typing import *


class DeviceTraitHandler:
    name: str
    multiple: bool
    description: str = "Undefined description."

    @classmethod
    def get_description(cls) -> str:
        return cls.description

    def __init__(self, trait: 'UsersDeviceTrait'):
        self.trait = trait


class MQTTDeviceTraitHandler(DeviceTraitHandler):
    default_value: Any
    schema = dict()
    multiple = True

    @classmethod
    def get_description(cls) -> str:
        desc = super().get_description()
        desc += '\nCan have multiple: ' + ('yes' if cls.multiple else 'no')
        if cls.default_value:
            desc += f'\nDefault value: {MQTTDeviceTraitHandler.dump_value(cls.default_value)}'
        if cls.schema:
            desc += f'\nJSON schema: {MQTTDeviceTraitHandler.dump_value(cls.schema)}'
        return desc

    @staticmethod
    def dump_value(value: Any) -> str:
        """
        Convert value to MQTT Payload
        """
        return json.dumps(value)

    def get_trait_value(self) -> str:
        """
        Get trait value in MQTT format.
        """
        return self.dump_value(self.trait.get_value())

    @classmethod
    def validate(cls, loaded: Any):
        if cls.schema:
            jsonschema.validate(loaded, cls.schema)

    @classmethod
    def load_value(cls, value: str) -> Any:
        try:
            loaded = json.loads(value)
            cls.validate(loaded)
            return loaded
        except (json.JSONDecodeError, jsonschema.ValidationError):
            raise ValueError(f'Unexpected MQTT value: {value}')

    def set_trait_value(self, value: str):
        """
        Db value from MQTT payload
        """
        raise NotImplementedError()

    def trait_value_html(self) -> str:
        """
        Render trait value as html.
        """
        return self.get_trait_value()


class AssistantDeviceTraitHandler(DeviceTraitHandler):
    commands: List[str] = list()
    multiple: bool = False
    compatible: Set[Type[MQTTDeviceTraitHandler]] = set()

    @classmethod
    def get_description(cls) -> str:
        desc = super().get_description()
        desc += '\nCan have multiple: ' + ('yes' if cls.multiple else 'no')
        return desc

    def get_trait_value(self):
        """
        Db value to specific assistant data format
        """
        raise NotImplementedError()

    def can_handle_command(self, command: str) -> bool:
        """
        Can this handler handle that command?
        """
        return command in self.commands

    def handle_command(self, command: str, payload: dict):
        """
        Executing specific commands
        """
        raise NotImplementedError()


class AmazonDeviceTraitHandler(AssistantDeviceTraitHandler, ABC):
    def get_supported_properties(self) -> List[Dict[str, Any]]:
        """
        List of supported capabilities used in discovery
        """
        raise NotImplementedError()

    @classmethod
    def get_configuration(cls) -> Dict[str, Any]:
        return dict()

    def get_trait_value(self) -> List[Dict[str, Any]]:
        raise NotImplementedError()


class GoogleDeviceTraitHandler(AssistantDeviceTraitHandler, ABC):
    def get_attributes(self) -> Dict[str, Any]:
        """
        List of trait attributes
        """
        return dict()

    def get_trait_value(self) -> Dict[str, Any]:
        raise NotImplementedError()
