import json

from flask_wtf import FlaskForm
from wtforms.fields import (
    BooleanField, SubmitField, PasswordField,
    StringField, TextAreaField, IntegerField, FloatField,
    DateField, DateTimeField, TimeField
)
from wtforms.validators import DataRequired, EqualTo
from flask_security import RegisterForm
from application.utils import split_string_list, join_string_list
from flask_restx import inputs
from flask_restx.reqparse import RequestParser
from wtforms.fields.core import UnboundField


class StringStripField(StringField):
    def __init__(self, *args, **kwargs):
        self.data = str()
        super().__init__(*args, **kwargs)

    def _value(self):
        return self.data.strip() if self.data else str()

    def process_formdata(self, value_list):
        self.data = value_list[0].strip()


class StringListField(StringField):
    def __init__(self, *args, **kwargs):
        self.data = list()
        super().__init__(*args, **kwargs)

    def _value(self):
        return join_string_list(self.data) if self.data else str()

    def process_formdata(self, value_list):
        self.data = split_string_list(value_list[0]) if value_list else list()


class JSONDictField(TextAreaField):
    def __init__(self, *args, **kwargs):
        self.data = dict()
        super().__init__(*args, **kwargs)

    def _value(self):
        return json.dumps(self.data or dict(), indent=4)

    def process_formdata(self, value_list):
        if value_list:
            if value_list[0].strip():
                try:
                    self.data = json.loads(value_list[0])
                    assert type(self.data) is dict
                except (json.JSONDecodeError, AssertionError):
                    raise ValueError('This field contains invalid data.')
        else:
            self.data = dict()

    def pre_validate(self, form):
        super().pre_validate(form)
        if self.data:
            try:
                json.dumps(self.data)
                assert type(self.data) is dict
            except (json.JSONDecodeError, AssertionError):
                raise ValueError('This field contains invalid data.')


class Form(FlaskForm):
    request_parser_skip_fields = [  # Fields to skip...
        SubmitField
    ]
    request_parser_type_map = {  # Defining Attribute types, etc...
        IntegerField: dict(type=str),
        FloatField: dict(type=float),
        BooleanField: dict(type=bool),
        JSONDictField: dict(type=dict),
        StringListField: dict(action='append'),
        DateField: dict(type=inputs.date),
        DateTimeField: dict(type=inputs.datetime_from_iso8601),
        TimeField: dict(type=inputs.mktime_tz)
    }

    @classmethod
    def get_request_parser(cls, help_format: str = '"{name}": {description}', **kwargs) -> RequestParser:
        """
        Api RequestParser from WTForm
        """
        parser = RequestParser(**kwargs)
        for name, value in cls.__dict__.items():
            # Must be UnboundField and field_class not in blacklisted
            if type(value) == UnboundField and value.field_class not in cls.request_parser_skip_fields:
                parser_kwargs = dict(  # Attribute kwargs
                    help=help_format.format(
                        name=name,
                        description=value.args[0]
                    ),
                    type=str,
                    required=any([  # Required by checking form validators
                        type(validator) is DataRequired
                        for validator in value.kwargs.get('validators', list())
                    ]),
                    default=value.kwargs.get('default')
                )
                # Setting Attribute types, etc...
                if type_kwargs := cls.request_parser_type_map.get(value.field_class):
                    parser_kwargs.update(type_kwargs)
                # Setting default by type if not defined in form
                if not parser_kwargs['default']:
                    try:
                        parser_kwargs['default'] = parser_kwargs['type']()
                    except:
                        pass
                parser.add_argument(name, **parser_kwargs)
        return parser


class ConfirmForm(Form):
    confirm = BooleanField('Confirm', validators=[DataRequired()])
    submit = SubmitField('Submit')


class ExtendedRegisterForm(RegisterForm):
    password_again = PasswordField('Password again', validators=[DataRequired(), EqualTo('password')])
    submit = RegisterForm.submit


class DeviceAddForm(Form):
    name = StringField('Device name', validators=[DataRequired()])
    description = StringField('Device description', validators=[DataRequired()])
    amazon_type = StringListField('Amazon type')
    google_type = StringStripField('Google type')
    custom_data = JSONDictField('JSON formatted device custom data (Add or edit only if you know what you are doing.)')
    submit = SubmitField('Add')


class DeviceEditForm(DeviceAddForm):
    submit = SubmitField('Edit')


class TraitAddForm(Form):
    mqtt_get = StringField('MQTT get topic')
    mqtt_set = StringField('MQTT set topic')
    readable = BooleanField('Readable')
    settable = BooleanField('Settable')
    custom_data = JSONDictField('JSON formatted trait custom data (Edit only if you know what you are doing.)')
    mqtt_handler = StringStripField('MQTT handler name', validators=[DataRequired()])
    amazon_handler = StringStripField('Amazon handler name')
    google_handler = StringStripField('Google handler name')
    submit = SubmitField('Add')


class TraitEditForm(TraitAddForm):
    submit = SubmitField('Edit')
