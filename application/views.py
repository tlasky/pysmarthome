from flask import request, redirect, url_for, render_template, abort, flash
from flask_security import current_user, login_required
from flask_restx import apidoc
from application import app, api, oauth_handler, oauth_required
from application.forms import *
from application.models import *
from application.webhook_handlers import amazon, google
from config import MQTT_HOST, MQTT_PORT


@app.route('/oauth/authorize', methods=['GET', 'POST'])
@login_required
def oauth_authorize():
    form = ConfirmForm(**request.form)
    if form.validate_on_submit():
        return oauth_handler.create_authorization_response(grant_user=current_user)
    grant = oauth_handler.validate_consent_request(end_user=current_user)
    # scopes = grant.client.get_allowed_scope(grant.request.scope)
    return render_template(
        'form.html',
        title=f'Allow access for {grant.client.name}',
        form=form,
    )


@app.route('/oauth/token', methods=['POST'])
def oauth_token():
    return oauth_handler.create_token_response()


@app.route('/fulfillment/<assistant>', methods=['POST'])
@oauth_required()
def fulfillment(assistant: str):
    request_dict = request.get_json()
    app.logger.debug(json.dumps(request_dict))
    if assistant == 'amazon':
        response_dict = amazon.handle(request_dict)
    elif assistant == 'google':
        response_dict = google.handle(request_dict)
    else:
        return abort(404)
    app.logger.debug(json.dumps(response_dict))
    return response_dict


@app.route('/')
def index():
    if current_user.is_authenticated:
        return redirect(url_for('dashboard'))
    return render_template('index.html')


@app.route('/tos')
@app.route('/privacy')
def tos_privacy():
    return render_template('tos_privacy.html')


@app.route('/dashboard')
@login_required
def dashboard():
    devices = current_user.devices.order_by(UsersDevice.name)
    return render_template(
        'dashboard.html',
        devices=devices,
        online_devices=[
            device
            for device in devices
            if device.online
        ],
        mqtt_host=MQTT_HOST,
        mqtt_port=MQTT_PORT,
        trait_controller=trait_controller
    )


@app.route('/api')
@api.documentation
@login_required
def api_doc():
    return apidoc.ui_for(api)


@app.route('/handlers')
@app.route('/handler/<handler_name>')
def handlers(handler_name: str = None):
    if not handler_name:
        return render_template(
            'handlers.html',
            trait_controller=trait_controller
        )
    if handler := trait_controller.get_handler(handler_name):
        return render_template(
            'handler.html',
            handler=handler,
            compatible=sorted(
                trait_controller.get_compatible(handler),
                key=lambda c: c.name
            )
        )
    abort(404)


@app.route('/device/<int:device_id>')
@login_required
def device_view(device_id: int):
    if not (device := UsersDevice.get(user=current_user, id=device_id)):
        return redirect(url_for('dashboard'))
    return render_template(
        'device.html',
        device=device,
        traits=device.traits.order_by(UsersDeviceTrait.id)
    )


@app.route('/device/add', methods=['GET', 'POST'])
@login_required
def device_add():
    form = DeviceAddForm(**request.form)
    if form.validate_on_submit():
        device = UsersDevice(
            name=form.name.data,
            description=form.description.data,
            user=current_user
        )
        form.populate_obj(device)
        db.commit()
        return redirect(url_for(
            'device_view',
            device_id=device.id
        ))
    return render_template(
        'form.html',
        title='Add device',
        message=render_template('device_types_hint.html'),
        form=form
    )


@app.route('/device/edit/<int:device_id>', methods=['GET', 'POST'])
@login_required
def device_edit(device_id: int):
    if not (device := UsersDevice.get(user=current_user, id=device_id)):
        return redirect(url_for('index'))
    if request.method == 'GET':
        form_data = device.to_dict()
    else:
        form_data = request.form
    form = DeviceEditForm(**form_data)
    if form.validate_on_submit():
        form.populate_obj(device)
        db.commit()
        return redirect(url_for('device_view', device_id=device.id))
    return render_template(
        'form.html',
        title='Edit device',
        message=render_template('device_types_hint.html'),
        form=form
    )


@app.route('/device/delete/<int:device_id>', methods=['GET', 'POST'])
@login_required
def device_delete(device_id: int):
    if not (device := UsersDevice.get(user=current_user, id=device_id)):
        return redirect(url_for('index'))
    form = ConfirmForm(**request.form)
    if form.validate_on_submit():
        device.delete()
        db.commit()
        return redirect(url_for('index'))
    return render_template(
        'form.html',
        title=f'Are you sure you want to delete device "{device.id}"',
        form=form
    )


@app.route('/device/<int:device_id>/trait/add', methods=['GET', 'POST'])
@login_required
def trait_add(device_id: int):
    if not (device := UsersDevice.get(user=current_user, id=device_id)):
        return redirect(url_for('index'))
    form = TraitAddForm(**request.form)
    if form.validate_on_submit():
        trait = UsersDeviceTrait(
            readable=form.readable.data,
            settable=form.settable.data,
            device=device
        )
        try:
            form.populate_obj(trait)
            db.commit()
            return redirect(url_for(
                'device_view',
                device_id=device.id
            ))
        except Exception as exc:
            flash(str(exc), 'warning')
            db.rollback()
    return render_template(
        'form.html',
        title='Add device trait',
        message=render_template(
            'trait_types_hint.html',
            trait_controller=trait_controller
        ),
        form=form
    )


@app.route('/device/<int:device_id>/trait/edit/<int:trait_id>', methods=['GET', 'POST'])
@login_required
def trait_edit(device_id: int, trait_id: int):
    if not (device := UsersDevice.get(id=device_id, user=current_user)):
        return redirect(url_for('index'))
    if not (trait := UsersDeviceTrait.get(device=device, id=trait_id)):
        return redirect(url_for('index'))
    if request.method == 'GET':
        form_data = trait.to_dict()
    else:
        form_data = request.form
    form = TraitEditForm(**form_data)
    if form.validate_on_submit():
        try:
            form.populate_obj(trait)
            db.commit()
            return redirect(url_for(
                'device_view',
                device_id=trait.device.id
            ))
        except Exception as exc:
            flash(str(exc), 'warning')
            db.rollback()
    return render_template(
        'form.html',
        title='Edit device trait',
        message=render_template(
            'trait_types_hint.html',
            trait_controller=trait_controller
        ),
        form=form
    )


@app.route('/device/<int:device_id>/trait/delete/<int:trait_id>', methods=['GET', 'POST'])
@login_required
def trait_delete(device_id: int, trait_id: int):
    if not (device := UsersDevice.get(id=device_id, user=current_user)):
        return redirect(url_for('index'))
    if not (trait := UsersDeviceTrait.get(device=device, id=trait_id)):
        return redirect(url_for('index'))
    form = ConfirmForm(**request.form)
    if form.validate_on_submit():
        trait.delete()
        db.commit()
        return redirect(url_for(
            'device_view',
            device_id=device.id
        ))
    return render_template(
        'form.html',
        title=f'Are you sure you want to delete trait "{trait.id}"',
        form=form
    )
