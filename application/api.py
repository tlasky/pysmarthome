from application import api
from application.models import *
from application.forms import *
from flask import jsonify
from flask_restx import Resource
from flask_security import login_required, current_user

device_namespace = api.namespace('device', description='Device operations')
trait_namespace = api.namespace('trait', description='Device traits operations')


@device_namespace.route('/<int:uid>')
class DeviceResource(Resource):
    device_parser = DeviceAddForm.get_request_parser(bundle_errors=True)

    @login_required
    def get(self, uid: int):
        if not (device := UsersDevice.get(id=uid, user=current_user)):
            api.abort(404, message='Device not found or it does not belong to you.')
        return jsonify(
            device=device.to_dict(
                exclude='user',
                with_collections=True,
                related_objects=True
            )
        )

    @api.expect(device_parser)
    @login_required
    def put(self, uid: int):
        if not (device := UsersDevice.get(id=uid, user=current_user)):
            api.abort(404, message='Device not found or it does not belong to you.')
        for key, value in self.device_parser.parse_args(strict=True).items():
            setattr(device, key, value)
        db.commit()
        return jsonify(
            device=device.to_dict(
                exclude='user',
                with_collections=True,
                related_objects=True
            )
        )

    @login_required
    def delete(self, uid: int):
        if not (device := UsersDevice.get(id=uid, user=current_user)):
            api.abort(404, message='Device not found or it does not belong to you.')
        device.delete()
        db.commit()
        return jsonify(
            device=None
        )


@device_namespace.route('/uid/<uid>')
class DeviceUidResource(Resource):
    def get(self, uid: str):
        if not (device := UsersDevice.get(uid=uid)):
            api.abort(404, message='Device not found.')
        return jsonify(
            device=device.to_dict(
                exclude='user',
                with_collections=True,
                related_objects=True
            )
        )


@device_namespace.route('/')
class DeviceListResource(Resource):
    device_parser = DeviceAddForm.get_request_parser(bundle_errors=True)

    @login_required
    def get(self):
        return jsonify(
            devices=[
                device.to_dict(
                    exclude='user',
                    with_collections=True,
                    related_objects=True
                )
                for device in current_user.devices
            ]
        )

    @api.expect(device_parser)
    @login_required
    def post(self):
        device = UsersDevice(
            user=current_user,
            **self.device_parser.parse_args(strict=True)
        )
        db.commit()
        return jsonify(
            device=device.to_dict(
                exclude='user',
                with_collections=True,
                related_objects=True
            )
        )


@trait_namespace.route('/<int:uid>')
class DeviceTraitResource(Resource):
    trait_parser = TraitAddForm.get_request_parser(bundle_errors=True)
    handlers_keys = ['mqtt_handler', 'amazon_handler', 'google_handler']

    @login_required
    def get(self, uid: int):
        if not (trait := UsersDeviceTrait.get(id=uid)) or trait.device.user != current_user:
            api.abort(404, message='Device trait not found or it does not belong to you.')
        return jsonify(
            trait=trait
        )

    @login_required
    def delete(self, uid: int):
        if not (trait := UsersDeviceTrait.get(id=uid)) or trait.device.user != current_user:
            api.abort(404, message='Device trait not found or it does not belong to you.')
        trait.delete()
        db.commit()
        return jsonify(
            trait=None
        )

    @api.doc(params={'uid': 'An device ID'})
    @api.expect(trait_parser)
    @login_required
    def post(self, **kwargs):
        if not (device := UsersDevice.get(id=kwargs.pop('uid'), user=current_user)):
            api.abort(404, message='Device not found or it does not belong to you.')
        request_args = self.trait_parser.parse_args(strict=True)
        try:
            trait = UsersDeviceTrait(
                device=device,
                **{
                    name: value
                    for name, value in request_args.items()
                    if name not in self.handlers_keys
                }
            )
            for key in self.handlers_keys:
                setattr(trait, key, request_args[key])
            db.commit()
            return jsonify(
                trait=trait
            )
        except Exception as exc:
            db.rollback()
            api.abort(400, message=str(exc))

    @api.expect(trait_parser)
    @login_required
    def put(self, **kwargs):
        if not (trait := UsersDeviceTrait.get(id=kwargs.pop('uid'))) or trait.device.user != current_user:
            api.abort(404, message='Device trait not found or it does not belong to you.')
        try:
            for key, value in self.trait_parser.parse_args(strict=True).items():
                setattr(trait, key, value)
            db.commit()
        except Exception as exc:
            db.rollback()
            api.abort(400, message=str(exc))
        return jsonify(
            trait=trait
        )
