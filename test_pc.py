"""
This script is virtual device used for remote shutdown of PC.
Warning: This will actually shut down yor PC.
"""
import os
import sys
import uuid
import json
import time
from config import MQTT_HOST, MQTT_PORT
from paho.mqtt.client import Client, MQTTMessage

mqtt = Client(f'TestPC{uuid.uuid4()}')

ping_get = '/tlasky/pc/ping/get'
power_get = '/tlasky/pc/power/get'
power_set = '/tlasky/pc/power/set'


def on_message(cl, ud, message: MQTTMessage):
    value = json.loads(message.payload.decode())
    if message.topic == power_set and not value:
        print('Shutting down')
        if 'win' in sys.platform:
            os.system('shutdown -s -t 0')  # Windows shutdown
        else:
            os.system('shutdown now')  # Linux (etc...) shutdown


mqtt.on_message = on_message
mqtt.connect(MQTT_HOST, MQTT_PORT)
mqtt.subscribe(power_set)
last_publish_at = 0

print('Waiting for shutdown command.')

try:
    while True:
        if last_publish_at == 0:
            mqtt.publish(power_get, json.dumps(True))
        if time.time() - last_publish_at > 2:  # To keep device online
            mqtt.publish(ping_get, json.dumps(str()))
            last_publish_at = time.time()
        mqtt.loop()
except KeyboardInterrupt:
    mqtt.disconnect()
