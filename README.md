# PySmartHome

Smart home project connecting MQTT, Amazon Alexa and Google Assistant together.
Example can be seen at [PySmartHome](https://pysmarthome.herokuapp.com/) website.
