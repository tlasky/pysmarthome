import os
import secrets
import logging

logging.basicConfig(level=logging.DEBUG)

base_path = os.path.dirname(os.path.abspath(__file__))
static_path = os.path.join(base_path, 'static')
templates_path = os.path.join(base_path, 'templates')
secret_key_path = os.path.join(base_path, 'secret_key.txt')
environ_database_url = os.environ.get('DATABASE_URL')

HEROKU = bool(os.environ.get('HEROKU'))
HOST = '0.0.0.0'
PORT = int(os.environ.get('PORT', 5000))
SECRET_KEY = os.environ.get('SECRET_KEY')
DEBUG = not HEROKU
TEMPLATES_AUTO_RELOAD = DEBUG

if not SECRET_KEY:
    if os.path.isfile(secret_key_path):
        SECRET_KEY = open(secret_key_path, 'r').read().strip()
    else:
        SECRET_KEY = secrets.token_urlsafe()
        open(secret_key_path, 'w').write(SECRET_KEY)

PONY = dict(
    provider=environ_database_url.split(':')[0],
    dsn=environ_database_url
) if environ_database_url else dict(
    provider='sqlite',
    filename=os.path.join(base_path, 'database.sqlite'),
    create_db=True
)

# Mail settings
MAIL_SERVER = 'smtp.seznam.cz'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = 'pysmarthome@post.cz'
MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')

# Security setting
SECURITY_TRACKABLE = True
SECURITY_DEFAULT_REMEMBER_ME = True
SECURITY_CONFIRMABLE = True
SECURITY_REGISTERABLE = True
SECURITY_RECOVERABLE = True
SECURITY_CHANGEABLE = True
SECURITY_EMAIL_SENDER = MAIL_USERNAME
SECURITY_PASSWORD_SALT = SECRET_KEY

WTF_CSRF_ENABLED = False
WTF_CSRF_CHECK_DEFAULT = False
SECURITY_CSRF_IGNORE_UNAUTH_ENDPOINTS = True
SECURITY_CSRF_PROTECT_MECHANISMS = ['session', 'basic']

# Because server is running behind a proxy, authlib needs to be insecure
os.environ['AUTHLIB_INSECURE_TRANSPORT'] = '1'

# MQTT broker
MQTT_HOST = 'broker.hivemq.com'
MQTT_PORT = 1883
MQTT_LISTENER_STANDALONE = not HEROKU

# ETC
DEVICE_RESPONSE_LIMIT = 10
