"""
Amazon AWS lambda script which is used to delegate requests to PySmartHome
"""

import json
import urllib3

http = urllib3.PoolManager()


def print_dict(data: dict, **kwargs):
    print(json.dumps(data, **kwargs))


def lambda_handler(request: dict, context):
    print_dict(request)
    token = 'INVALID'
    for entry_key in ['endpoint', 'payload']:
        if entry := request['directive'].get(entry_key):
            if scope := entry.get('scope'):
                token = scope['token']
    http_response = http.request(
        'POST',
        'https://pysmarthome.herokuapp.com/fulfillment/amazon',
        body=json.dumps(request),
        headers={
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {token}'
        }
    )
    response = json.loads(http_response.data.decode())
    print_dict(response)
    return response
