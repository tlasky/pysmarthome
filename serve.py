from application import app, mqtt_listener
from config import HOST, PORT, DEBUG, MQTT_LISTENER_STANDALONE

try:
    if not MQTT_LISTENER_STANDALONE:
        mqtt_listener.start()
    if DEBUG:
        print('Running on development WSGI')
        app.run(HOST, PORT)
    else:
        print('Running on production WSGI')
        from gevent.pywsgi import WSGIServer

        WSGIServer((HOST, PORT), app).serve_forever()
except KeyboardInterrupt:
    pass
finally:
    if not MQTT_LISTENER_STANDALONE:
        mqtt_listener.stop()
