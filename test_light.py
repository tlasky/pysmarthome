"""
This script is virtual light used for testing.

"""

import uuid
import json
import time
from typing import *
from config import MQTT_HOST, MQTT_PORT
from paho.mqtt.client import Client, MQTTMessage

mqtt = Client(f'TestLight{uuid.uuid4()}')

ping_get = '/pysmarthome/test/ping/get'
power = False
power_get = '/pysmarthome/test/power/get'
power_set = '/pysmarthome/test/power/set'
brightness = 100
brightness_get = '/pysmarthome/test/brightness/get'
brightness_set = '/pysmarthome/test/brightness/set'
color = [255, 255, 255]
color_get = '/pysmarthome/test/color/get'
color_set = '/pysmarthome/test/color/set'


def on_message(cl, ud, message: MQTTMessage):
    global power, brightness, color
    value = json.loads(message.payload.decode())
    if message.topic == power_set:
        power = value
    elif message.topic == brightness_set:
        brightness = value
    elif message.topic == color_set:
        color = value


mqtt.on_message = on_message
mqtt.connect(MQTT_HOST, MQTT_PORT)
mqtt.subscribe(power_set)
mqtt.subscribe(brightness_set)
mqtt.subscribe(color_set)
last_publish_at = 0


def get_visual(brightness: int, color: List[int]) -> str:
    chars = '░▒▓█'
    index = int(brightness * (len(chars) - 1) / 100)
    r, g, b = color
    return f'\x1b[38;2;{r};{g};{b}m{chars[index] * 2}\x1b[0m'


try:
    while True:
        if last_publish_at == 0:
            mqtt.publish(power_get, json.dumps(power))
            mqtt.publish(brightness_get, json.dumps(brightness))
            mqtt.publish(color_get, json.dumps(color))
        if time.time() - last_publish_at > 2:  # To keep device online
            mqtt.publish(ping_get, json.dumps(str()))
            last_publish_at = time.time()
        print(
            '\rTest light '
            'power: {:>3} '
            'brightness: {:>3} '
            'color: [{:>3}, {:>3}, {:>3}] '
            'visual: {}'.format(
                'ON' if power else 'OFF',
                brightness,
                *color,
                get_visual(brightness, color if power else [0, 0, 0])
            ),
            end=str()
        )
        mqtt.loop()
except KeyboardInterrupt:
    mqtt.disconnect()
